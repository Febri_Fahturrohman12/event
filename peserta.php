<?php

$servername = "localhost";
$username = "u663794328_event";
$password = "Bismillah1407*";
$dbname = "u663794328_event";
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


$sql = "SELECT * FROM peserta";
$result = $conn->query($sql);


?>
<!DOCTYPE html>

<html lang="en">


<!-- Mirrored from expert-themes.com/html/eventrox/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 21 Nov 2021 03:51:30 GMT -->
<head>

    <meta charset="utf-8">

    <title>Peserta Cryptoday, Gerbang Menuju Dunia Hacker</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <br/>
            <h3>Peserta Cryptoday, Gerbang Menuju Dunia Hacker</h3><br/>
        </div>

        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col" class="text-center">#</th>
                    <th scope="col" >Nama</th>
                    <th scope="col" >Asal Kota</th>
                    <th scope="col">Asal Sekolah</th>
                    <th scope="col" class="text-center">Register Pada</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if ($result->num_rows > 0) {
                    // output data of each row
                    $no =1;
                    while ($row = $result->fetch_assoc()) {
                        ?>
                        <tr>
                            <th scope="row" class="text-center"><?= $no ?></th>
                            <td><?= $row["nama"] ?></td>
                            <td><?= $row["asal_kota"] ?></td>
                            <td><?= $row["asal_sekolah"] ?></td>
                            <td class="text-center"><?= date("d M Y, H:i",strtotime($row["created_at"])) ?></td>
                        </tr>
                        <?php
                        $no++;
                    }
                } else {
                    echo "0 results";
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>
</html>
